#include "cooley_turkey.h"
#include <stdlib.h>
#include <stdio.h>

/* Twiddle-di and twiddle-do output a static
 * table of twiddle factors for cooley-turkey
  * FFT function */
f_complex * twiddles_factors(int N, f_complex * tf) {
	int i;
	if(N%2 != 0) {
		return 0;
	}
	for(i = 0; i < N/2; i++) {
		// e(-2*PI*i/N)
		tf[i] = c_r_polar(1, -2.0*PI*i/N);
	}
	return tf;
}

f_complex *fft_init_mem_X(int N) {
	int size = 0;
	int i;
	f_complex *mem_array = 0;

	for(i = N; i>=1 ; i=i/2) {
		size += i;
		//printf("i: %d\n", i);
	}
	//printf("Size: %d, total size: %d\n", size, size * sizeof(struct f_complex_t));
	mem_array = malloc(sizeof(struct f_complex_t) * size);
	return mem_array;
}

f_complex *fft_init_mem_ed(int N) {
	int size = 0;
	int i;
	f_complex *mem_array = 0;

	for(i = N/2; i>1; i=i/2) {
		size += i;
		//printf("i: %d\n", i);
	}
	//printf("Size: %d, total size: %d\n", size, size * sizeof(struct f_complex_t));
	mem_array = malloc(sizeof(struct f_complex_t) * size);
	return mem_array;
}

f_complex *fft_basic(f_complex *samples, int N) {
	int k;
	int factor;
	f_complex * e, * d, * D, *E;
	f_complex *X = (f_complex *)malloc(sizeof(struct f_complex_t) * N);
	if(N==1) {
		X[0] = samples[0];
		return X;
	}
	e = (f_complex *)malloc(sizeof(struct f_complex_t) * N/2);
	d = (f_complex *)malloc(sizeof(struct f_complex_t) * N/2);
	for(k = 0; k < N/2; k++) {
		e[k] = samples[2*k];
		d[k] = samples[2*k+1];
	}
	E = fft_basic(e, N/2);
	D = fft_basic(d, N/2);

	for(k = 0; k < N/2; k++) {
		D[k] = c_mul(c_r_polar(1, -2.0*PI*k/N),D[k]);
	}
	for(k = 0; k < N/2; k++) {
		X[k] = c_add(E[k], D[k]);
		X[k+N/2] = c_sub(E[k], D[k]);
	}
	return X;
}

f_complex *fft_tf_static(f_complex *samples, int N) {
	int k;
	int factor;
	f_complex * e, * d, * D, *E;
	f_complex *X = (f_complex *)malloc(sizeof(struct f_complex_t) * N);
	if(N==1) {
		X[0] = samples[0];
		return X;
	}
	e = (f_complex *)malloc(sizeof(struct f_complex_t) * N/2);
	d = (f_complex *)malloc(sizeof(struct f_complex_t) * N/2);
	for(k = 0; k < N/2; k++) {
		e[k] = samples[2*k];
		d[k] = samples[2*k+1];
	}
	E = fft_tf_static(e, N/2);
	D = fft_tf_static(d, N/2);

	factor = nb_samples/N;
	for(k = 0; k < N/2; k++) {
		D[k] = c_mul(tf_tab[k*factor], D[k]);
	}
	for(k = 0; k < N/2; k++) {
		X[k] = c_add(E[k], D[k]);
		X[k+N/2] = c_sub(E[k], D[k]);
	}
	return X;
}

f_complex *fft_unrolled_memory(f_complex *samples, int N, int depth) {
	int k;
	int factor;
	f_complex * e, * d, * D, *E;
	f_complex *X = (f_complex *)malloc(sizeof(struct f_complex_t) * N);
	if(N==1) {
		X[0] = samples[0];
		return X;
	}
	e = (f_complex *)malloc(sizeof(struct f_complex_t) * N/2);
	d = (f_complex *)malloc(sizeof(struct f_complex_t) * N/2);
	for(k = 0; k < N/2; k++) {
		e[k] = samples[2*k];
		d[k] = samples[2*k+1];
	}
	E = fft_unrolled_memory(e, N/2, depth - 1);
	D = fft_unrolled_memory(d, N/2, depth - 1);

	factor = nb_samples/N;
	for(k = 0; k < N/2; k++) {
		D[k] = c_mul(tf_tab[k*factor], D[k]);
	}
	for(k = 0; k < N/2; k++) {
		X[k] = c_add(E[k], D[k]);
		X[k+N/2] = c_sub(E[k], D[k]);
	}
	return X;
}
