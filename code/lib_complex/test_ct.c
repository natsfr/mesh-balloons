#include "cooley_turkey.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main () {
	int i;
	f_complex *cos_samples, *cos_samples_2;
	f_complex *results;
	//results = (f_complex *)malloc(sizeof(struct f_complex_t) * nb_samples);

	/*main_X = fft_init_mem_X(nb_samples);
	main_e = fft_init_mem_ed(nb_samples);
	main_d = fft_init_mem_ed(nb_samples);*/

	/* generate a sinus at 1/128 of the fs */
	cos_samples = (f_complex *)malloc(sizeof(struct f_complex_t) * nb_samples);
	cos_samples_2 = (f_complex *)malloc(sizeof(struct f_complex_t) * nb_samples);
	for(i = 0; i < nb_samples; i++) {
		cos_samples[i] = c_r_polar(1, -10*PI*i/nb_samples);
		cos_samples_2[i] = c_r_polar(1.5, (50*PI*i-i)/nb_samples);
		cos_samples[i] = c_add(cos_samples[i], cos_samples_2[i]);
		//printf("i: %d - ci: %f +j %f\n", i, cos_samples[i].R, cos_samples[i].I);
	}

	/* generate tf table */
	tf_tab = (f_complex*)malloc(sizeof(struct f_complex_t) * nb_samples/2);
	twiddles_factors(nb_samples, tf_tab);

	results = fft_basic(cos_samples, nb_samples);
	
	for(i = 0; i < nb_samples; i++) {
		printf("basic i: %d - sample %f +j %f\n", i, results[i].R, results[i].I);
	}

	results = fft_tf_static(cos_samples, nb_samples);

	for(i = 0; i < nb_samples; i++) {
		printf("tf_static i: %d - sample %f +j %f\n", i, results[i].R, results[i].I);
	}


	return 0;
}

