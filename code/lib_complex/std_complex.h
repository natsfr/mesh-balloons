#ifndef std_complex
#define std_complex

#define PI 3.141592654

typedef struct f_complex_t{
	float R;
	float I;
} f_complex;

// Complex vs Complex functions
f_complex c_add(f_complex a, f_complex b);
f_complex c_sub(f_complex a, f_complex b);
f_complex c_mul(f_complex a, f_complex b);
f_complex c_conj(f_complex a);
f_complex c_div(f_complex a, f_complex b);
f_complex c_r_mul(f_complex a, float r);
// Complex vs Real functions
f_complex c_r_polar(float r, float rad);
f_complex c_r_div(f_complex a, float b);
f_complex c_r_mul(f_complex a, float b);

// IQ Processing functions
// to be optimized for target
float c_amp(f_complex sample);
float c_phi(f_complex sample);

// General Electronic functions
f_complex c_par_imp(f_complex Z1, f_complex Z2);
#endif
