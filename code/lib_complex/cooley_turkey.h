#ifndef cooturk
#define cooturk

#include "std_complex.h"

f_complex * twiddles_factors(int N, f_complex * tf);
f_complex * fft_init_mem_X(int N);
f_complex * fft_init_mem_ed(int N);
f_complex * fft_optimized(f_complex *samples, int N);

f_complex *main_X;
f_complex *main_e;
f_complex *main_d;
f_complex *tf_tab;

#define nb_samples 4096

/*
 * used to pregenerate all the tables
 */


#endif
