#include <stdio.h>
#include <math.h>

#include "std_complex.h"

f_complex c_add(f_complex a, f_complex b) {
	f_complex tmp;
	tmp.R = a.R + b.R;
	tmp.I = a.I + b.I;
	return tmp;
}

f_complex c_sub(f_complex a, f_complex b) {
	f_complex tmp;
	tmp.R = a.R - b.R;
	tmp.I = a.I - b.I;
	return tmp;
}

/* Should be optimized */
f_complex c_mul(f_complex a, f_complex b) {
	f_complex tmp;
	tmp.R = (a.R*b.R)+(-1*a.I*b.I);
	tmp.I = (a.R*b.I)+(a.I*b.R);
	return tmp;
}

f_complex c_conj(f_complex a) {
	f_complex tmp;
	tmp.R = a.R;
	tmp.I = -1 * a.I;
	return tmp;
}

/* Should be optimized */
f_complex c_div(f_complex a, f_complex b) {
	f_complex tmp;
	f_complex denom = c_mul(b, c_conj(b)); // <= this is dumb
                                           // denom = b.R² + b.I²
	f_complex num = c_mul(a, c_conj(b));
	tmp.R = num.R / denom.R;
	tmp.I = num.I / denom.R;
	return tmp;
}

f_complex c_r_mul(f_complex a, float b) {
	f_complex tmp;
	tmp.R = a.R * b;
	tmp.I = a.I * b;
	return tmp;
}

f_complex c_r_div(f_complex a, float b) {
	f_complex tmp;
	tmp.R = a.R / b;
	tmp.I = a.I / b;
	return tmp;
}

float c_amp(f_complex sample) {
	return sqrtf((sample.I*sample.I)+(sample.R*sample.R));
}

/* Paralleles impedances = 1/Z1 + 1/Z2
 * conj(Z1) / (Z1.R² + Z1.I²) + conj(Z2) / (Z2.R² + Z2.I²
 */
f_complex c_par_imp(f_complex Z1, f_complex Z2) {
	float denom1 = (Z1.R * Z1.R) + (Z1.I * Z1.I);
	float denom2 = (Z2.R * Z2.R) + (Z2.I * Z2.I);

	// this part needs to be optimised following
	// div operation cycles
	f_complex Y =  c_add(c_r_div(c_conj(Z1), denom1), c_r_div(c_conj(Z2), denom2));
	f_complex Z = c_r_div(c_conj(Y),((Y.I*Y.I)+(Y.R*Y.R)));
	return Z;
}

/* Take radian and give complex */
f_complex c_r_polar(float r, float rad) {
	f_complex tmp;
	tmp.R = r * cosf(rad);
	tmp.I = r * sinf(rad);
	return tmp;
}
