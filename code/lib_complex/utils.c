#include <stdio.h>
#include "utils.h"
#include "std_complex.h"
#include <math.h>

#define PI 3.141592654

void fft_generate_data(int N, int depth) {
	int k;
	depth++;
	
	f_complex * e, * d, * D, *E;
	//f_complex * X = (f_complex*)(main_X+(nb_samples-N)*2);
	//f_complex *X = (f_complex *)malloc(sizeof(struct f_complex_t) * N);
	printf("Depth: %d - nb_samples: %d - X_tab size: %d\n", depth, N, sizeof(struct f_complex_t) * N);
	if(N==1) {
		return;
	}
	printf("Depth: %d - nb_samples: %d - d_tab size: %d\n", depth, N, sizeof(struct f_complex_t) * N/2);
	printf("Depth: %d - nb_samples: %d - e_tab size: %d\n", depth, N, sizeof(struct f_complex_t) * N/2);
	//e = (f_complex *)malloc(sizeof(struct f_complex_t) * N/2);
	//d = (f_complex *)malloc(sizeof(struct f_complex_t) * N/2);
	
	fft_generate_data(N/2, depth);
	fft_generate_data(N/2, depth);
	//for(k = 0; k < N/2; k++) {
		//D[k] = c_mul(tf_tab[k], D[k]);
		//D[k] = c_mul(c_r_polar(1, -2.0*PI*k/N),D[k]);
	//}
}

void generate_fft_table() {
	int nb_floor = log2(NB_SAMPLE);
	int totalsize = 0;
	
	for (int i = nb_floor; i >= 0; i--) {
		int size = (powf(2, i) * sizeof(struct f_complex_t)); // size of 1 leave at level i = 2^i
		printf("//current_floor: %d - size: %d - 1 array: %d - 2 arrays: %d\n", i, (int)powf(2, i), size, size/2);
		totalsize += size*2;
	}
	printf("//Total size: %d bytes\n", totalsize);
	
	printf("#ifndef fft_space_h\n#define fft_space_h\n\n");
	printf("struct main_x {\n");
	for(int i = nb_floor; i >= 0; i--) {
		int size = (powf(2, i));
		printf("	f_complex %d[%d];\n", i, size);
	}
	printf("};\n");
	printf("struct main_e {\n");
	for(int i = nb_floor; i >= 0; i--) {
		int size = (powf(2, i)) / 2;
		printf("	f_complex %d[%d];\n", i, size);
	}
	printf("};\n");
	printf("struct main_d {\n");
	for(int i = nb_floor; i >= 0; i--) {
		int size = (powf(2, i)) / 2;
		printf("	f_complex %d[%d];\n", i, size);
	}
	printf("};\n\n");
	printf("#endif\n");
}

int main() {
	generate_fft_table();
	return 0;
}
