#include <stdio.h>
#include <math.h>
#include "std_complex.h"

int main() {
	f_complex a;
	a.R = 1;
	a.I = 2;
	f_complex b;
	b.R = 2;
	b.I = 3;

	printf("conj(a) = %f +i %f\n", c_conj(a).R, c_conj(a).I);
	printf("mul(a,b) = %f +i %f\n", c_mul(a,b).R, c_mul(a,b).I);
	printf("div(a,b) = %f +i %f\n", c_div(a,b).R, c_div(a,b).I);
	printf("amplitude a = %f\n", c_amp(a));
	f_complex Z1;
	Z1.R = 50;
	Z1.I = 0;
	f_complex Z2;
	Z2.R = 0;
	Z2.I = -150;
	f_complex Ze = c_par_imp(Z1,Z2);
	printf(" 50+J0 // 0-J150 = %f +j %f\n", Ze.R, Ze.I);
	return 0;
}

void test_fft() {
    


}