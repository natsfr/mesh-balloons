PCBNEW-LibModule-V1  03/12/2013 13:47:11
# encoding utf-8
Units mm
$INDEX
XTAL_50x32
$EndINDEX
$MODULE XTAL_50x32
Po 0 0 0 15 529DD2B7 00000000 ~~
Li XTAL_50x32
Sc 0
AR 
Op 0 0 0
T0 0 -3.5 1 1 0 0.15 N V 21 N "XTAL_50x32"
T1 0 3.5 1 1 0 0.15 N V 21 N "VAL**"
DS -3 -2 3 -2 0.15 21
DS 3 -2 3 2 0.15 21
DS 3 2 -3 2 0.15 21
DS -3 2 -3 -2 0.15 21
$PAD
Sh "1" R 1 1.2 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.25 1.25
$EndPAD
$PAD
Sh "2" R 1 1.2 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.25 1.25
$EndPAD
$PAD
Sh "3" R 1 1.2 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.25 -1.25
$EndPAD
$PAD
Sh "4" R 1 1.2 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.25 -1.25
$EndPAD
$EndMODULE XTAL_50x32
$EndLIBRARY
